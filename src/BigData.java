import java.util.Scanner;
import java.math.*;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.*;

public class BigData {

	public static String from10to27(BigInteger number) {
		
		BigInteger value = number;
		BigInteger cc = new BigInteger("27");
		String mCHAR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		BigInteger q = value.remainder(cc);
		while (  (value.remainder(cc).compareTo(BigInteger.ZERO) > 0)) {
			BigInteger p = value.divide(cc);
			q = value.remainder(cc);
			char ch = mCHAR.charAt(q.intValue());
			result = ch + result;
			value = p;
		}
		return result;
	}
	
	public static String from10to30(BigInteger number) {
		
		BigInteger value = number;
		BigInteger cc = new BigInteger("30");
		String mCHAR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		BigInteger q = value.remainder(cc);
		while (  (value.remainder(cc).compareTo(BigInteger.ZERO) > 0)) {
			BigInteger p = value.divide(cc);
			q = value.remainder(cc);
			char ch = mCHAR.charAt(q.intValue());
			result = ch + result;
			value = p;
		}
		return result;
	}
	
	public static BigInteger getSum (BigInteger b,BigInteger c, BigInteger d, BigInteger e)
	{
		BigInteger s=BigInteger.valueOf(0);
		s = b.add(c).add(d).add(e);
		return s;
	}
	
	public static void main(String[] args) {
	    int a=0;
        BigInteger b,c,d,e,z,p,l,u;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество переменных = ");
		if (scanner.hasNextInt()) {
			a=scanner.nextInt();
			System.out.println("Выбранное количество переменных = "+a);}
		else {
			System.out.print("Введены неверные данные ");
		}
		if(a==2) {
			d=BigInteger.valueOf(0);
			e=BigInteger.valueOf(0);
			System.out.println("Введите переменные");
			System.out.print("Первая переменная = ");
			if (scanner.hasNextBigInteger()) {
				b=scanner.nextBigInteger();
			}
			else {
				System.out.print("Введены неверные данные ");
				return;
			}
			System.out.print("Вторая переменная = ");
			if (scanner.hasNextBigInteger()) {
				c=scanner.nextBigInteger();
			}
			else {
				System.out.print("Введены неверные данные ");
				return;
			}
			BigInteger[] arr = {b, c};
		    int max = 0;
		    for (BigInteger j : arr) {
		        int k = j.toString().length();
		        if (k > max) max = k;
		     }
		     String str = "%0" + max + "d\n";
		     System.out.println("Переменные по порядку введения:");
		     for(BigInteger j: arr) {
		         System.out.format(str, j);
		     }
		}
		else {
			if(a==3) {
				e=BigInteger.valueOf(0);
				System.out.println("Введите переменные");
				System.out.print("Первая переменная = ");
				if (scanner.hasNextBigInteger()) {
					b=scanner.nextBigInteger();
				}
				else {
					System.out.print("Введены неверные данные ");
					return;
				}
				System.out.print("Вторая переменная = ");
				if (scanner.hasNextBigInteger()) {
					c=scanner.nextBigInteger();
				}
				else {
					System.out.print("Введены неверные данные ");
					return;
				}
				System.out.print("Третья переменная = ");
				if (scanner.hasNextBigInteger()) {
					d=scanner.nextBigInteger();
				}
				else {
					System.out.print("Введены неверные данные ");
					return;
				}
				BigInteger[] arr = {b, c, d};
			    int max = 0;
			    for (BigInteger j : arr) {
			        int k = j.toString().length();
			        if (k > max) max = k;
			     }
			     String str = "%0" + max + "d\n";
			     System.out.println("Переменные по порядку введения:");
			     for(BigInteger j: arr) {
			         System.out.format(str, j);
			     }
			}
			else {
				if(a==4) {
					System.out.println("Введите переменные");
					System.out.print("Первая переменная = ");
					if (scanner.hasNextBigInteger()) {
						b=scanner.nextBigInteger();
					}
					else {
						System.out.print("Введены неверные данные ");
						return;
					}
					System.out.print("Вторая переменная = ");
					if (scanner.hasNextBigInteger()) {
						c=scanner.nextBigInteger();
					}
					else {
						System.out.print("Введены неверные данные ");
						return;
					}
					System.out.print("Третья переменная = ");
					if (scanner.hasNextBigInteger()) {
						d=scanner.nextBigInteger();
					}
					else {
						System.out.print("Введены неверные данные ");
						return;
					}
					System.out.print("Четвёртая переменная = ");
					if (scanner.hasNextBigInteger()) {
						e=scanner.nextBigInteger();
					}
					else {
						System.out.print("Введены неверные данные ");
						return;
					}
					BigInteger[] arr = {b, c, d, e};
				    int max = 0;
				    for (BigInteger j : arr) {
				        int k = j.toString().length();
				        if (k > max) max = k;
				     }
				     String str = "%0" + max + "d\n";
				     System.out.println("Переменные по порядку введения:");
				     for(BigInteger j: arr) {
				         System.out.format(str, j);
				     }
				}
				else {
					System.out.println("Неверное количество переменных");
					return;
				}
			}
		}
		z=BigData.getSum(b, c, d, e);
		System.out.println("Сумма в десятичной системе счисления = "+BigData.getSum(b, c, d, e));
		p=BigInteger.valueOf(0);
		if(z.compareTo(p)==0)
		{
			System.out.println("Сумма в двадцатисемеричной системе счисления = 0");
		}
		else{
		if(z.compareTo(p)<0)
		{
			u=BigInteger.valueOf(-1);
			l=z.multiply(u);
			System.out.println("Сумма в двадцатисемеричной системе счисления = -"+from10to27(l));
			System.out.println("Сумма в тридцатиричной системе счисления = -"+from10to30(l));
		}
		else{
			System.out.println("Сумма в двадцатисемеричной системе счисления = "+from10to27(z));
			System.out.println("Сумма в тридцатиричной системе счисления = "+from10to30(z));
		}
		}
		
	}
	
}
