import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;


public class BigDataTest {

	@Test
	public void testGetSum() {
		{
			BigInteger t1 =BigInteger.valueOf(34);
			BigInteger t2 =BigInteger.valueOf(35);
			BigInteger t3 =BigInteger.valueOf(31215);
			BigInteger t4 =BigInteger.valueOf(5436344);
			BigInteger res = BigInteger.valueOf(5467628);
			assertEquals(BigData.getSum(t1, t2, t3, t4), res);
		} 
		
	}
	@Test
	public void testGetSum2() {
		{
			BigInteger t1 =BigInteger.valueOf(3123);
			BigInteger t2 =BigInteger.valueOf(33);
			BigInteger t3 =BigInteger.valueOf(15);
			BigInteger t4 =BigInteger.valueOf(7636344);
			BigInteger res = BigInteger.valueOf(7639515);
			assertEquals(BigData.getSum(t1, t2, t3, t4), res);
		} 
		
	}
	@Test
	public void testTo27() {
	    {
		    BigInteger t1 = BigInteger.valueOf(1662008);
		    String res = "33BMN";
		    assertEquals(BigData.from10to27(t1),res);
	    }
	}
	@Test
	public void testTo30() {
	    {
		    BigInteger t1 = BigInteger.valueOf(34251976);
		    String res = "1C8HMG";
		    assertEquals(BigData.from10to30(t1),res);
	    }
	}
}
